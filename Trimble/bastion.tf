resource "aws_instance" "bastion" {
  ami   = "ami-0b0dcb5067f052a63"
  instance_type = "t2.micro"
  key_name      = "mykey"
  subnet_id     = "${aws_subnet.pub-subnets[0].id}"
  vpc_security_group_ids = ["${aws_security_group.sg1.id}", "${aws_security_group.sg2.id}"]
  associate_public_ip_address = true
  root_block_device {
    volume_type = "gp2"
    volume_size = "8"
    delete_on_termination = true
  }
tags = {
    Name = "bastion"
  }
}
