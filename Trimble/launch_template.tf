resource "aws_launch_template" "template" {
  name                  = "ec2-template"
  image_id               = "ami-0b0dcb5067f052a63"
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.sg1.id}", "${aws_security_group.sg2.id}"]
  update_default_version = true
  user_data              = filebase64("http.sh")
  key_name               = "mykey"

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "Trimble-App"
    }
  }
}
